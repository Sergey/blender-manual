

****************
Preview Snapping
****************

The icon toggles snapping;
you can also do this temporarily by holding :kbd:`Ctrl` after starting to transform an image.

Images have multiple snap points; they can snap along their edges, corners, or center.

The drop-down arrow offers the following options:

Snap to
   .. _bpy.types.SequencerToolSettings.snap_to_borders:

   Borders
      Snap images to the edges of the render region.

   .. _bpy.types.SequencerToolSettings.snap_to_center:

   Center
      Snap images to the horizontal and vertical center lines of the render region.

   .. _bpy.types.SequencerToolSettings.snap_to_strips_preview:

   Other Strips
      Snap images to the snap points of other images.
