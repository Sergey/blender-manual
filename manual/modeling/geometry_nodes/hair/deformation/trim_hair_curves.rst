.. index:: Geometry Nodes; Trim Hair Curves

****************
Trim Hair Curves
****************

Trims or scales hair curves to a certain length.

.. peertube:: qfXRAqbL8MKus8YKMkUpE5


Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Scale Uniform
   Scale each curve uniformly to reach the target length.

Length Factor
   Multiply the original length by a factor.

Replace Length
   Use the length input to fully replace the original length.

Length
   Target length for the operation.

Mask
   Mask to blend overall effect.

Random Offset
   Trim hair curves randomly up to a certain amount.

Pin at Parameter
   Pin each curve at a certain point for the operation.

Seed
   Random Seed for the operation.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**
