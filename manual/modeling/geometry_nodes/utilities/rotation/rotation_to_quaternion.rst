.. index:: Geometry Nodes; Rotation to Quaternion
.. _bpy.types.FunctionNodeRotationToQuaternion:

***************************
Rotation to Quaternion Node
***************************

.. figure:: /images/node-types_FunctionNodeRotationToQuaternion.webp
   :align: right
   :alt: Rotation to Quaternion node.

The *Rotation to Quaternion* node converts a standard rotation value to a
:ref:`quaternion rotation <quaternion mode>`.

Inputs
======

Rotation
    Standard rotation value.


Outputs
=======

W
    The W value of the quaternion.
X
    The X value of the quaternion.
Y
    The Y value of the quaternion.
Z
    The Z value of the quaternion.
