.. index:: Geometry Nodes; Mix
.. DO NOT EDIT FILE. This is simply a stub which copies everything from the link below.

********
Mix Node
********

.. include:: /render/shader_nodes/converter/mix.rst
   :start-after: .. --- copy below this line ---
