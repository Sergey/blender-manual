.. _bpy.ops.transform.push_pull:
.. _tool-transform-push_pull:

*********
Push/Pull
*********

.. reference::

   :Mode:      Object and Edit Modes
   :Tool:      :menuselection:`Toolbar --> Shrink/Fatten --> Push/Pull`
   :Menu:      :menuselection:`Object/Mesh --> Transform --> Push/Pull`

.. figure:: /images/modeling_meshes_editing_mesh_transform_push-pull_operator-panel.png
   :align: right

   Push/Pull distance.

Moves the selected elements closer to (Push) or further away from (Pull) the pivot point,
all by the same distance. You can control this distance by moving the mouse up or down,
typing a number, or using the slider in the :ref:`bpy.ops.screen.redo_last` panel.


Examples
========

.. figure:: /images/modeling_meshes_editing_mesh_transform_push-pull_objects-equidistant.png

   Equidistant objects being pushed together.

.. figure:: /images/modeling_meshes_editing_mesh_transform_push-pull_objects-random.png

   Random objects being pushed together.

.. figure:: /images/modeling_meshes_editing_mesh_transform_push-pull_vertices.png

   Push (middle) vertices around the 3D cursor compared to Scale (right).
