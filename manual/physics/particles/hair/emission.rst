
********
Emission
********

.. reference::

   :Panel:     :menuselection:`Particle System --> Emission`

.. TODO2.8:
   .. figure:: /images/physics_particles_hair_emission_settings.png

      Hair particle system settings.

Number
   Sets the amount of hair strands. Use as few particles as possible
   (especially if you plan to use soft body animation later),
   but still enough to cover the surface and have good control.
   A few thousand particles is generally enough for a regular haircut.
   The hair will be made denser later on using
   :doc:`/physics/particles/emitter/children`.
Hair Length
   Controls the length of the hair.

.. seealso::

   Emitter particles :doc:`Emission panel </physics/particles/emitter/emission>`
