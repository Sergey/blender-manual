
******************************
Scalable Vector Graphics (SVG)
******************************

.. reference::

   :Category:  Import-Export
   :Menu:      :menuselection:`File --> Import --> Scalable Vector Graphics (.svg)`

.. note::

   Currently the script allows only importing and is limited to path geometry only.


Enabling Add-on
===============

This add-on is enabled by default, in case it is not:

#. Open Blender and go to :doc:`/editors/preferences/addons` section of the :doc:`/editors/preferences/index`.
#. Search "Scalable Vector Graphics (SVG)" and check the *Enable Extension* checkbox.


Properties
==========

This add-on does not have any properties.


Usage
=====

Todo.
