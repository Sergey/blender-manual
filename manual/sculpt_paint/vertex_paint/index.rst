.. _painting-vertex-index:
.. _bpy.types.VertexPaint:
.. _bpy.types.VertexColors:

################
  Vertex Paint
################

.. toctree::
   :maxdepth: 2

   introduction.rst
   brushes.rst
   tools.rst
   tool_settings/index.rst
   editing.rst
