
*******
Toolbar
*******

The amount of tools in sculpt mode is very extensive.
This is an overview of all of them, categorized by their general functions.


Sculpting Tools
===============

.. figure:: /images/sculpt-paint_sculpting_toolbar_brush.png
   :align: right
   :width: 220

:doc:`/sculpt_paint/sculpting/tools/brush_tool`
   Tool to use for any of the *Sculpt* mode brushes.


Gesture Tools
=============

.. figure:: /images/sculpt-paint_sculpting_toolbar_gestures.png
   :align: right

General gesture tools to apply an operation via box, lasso, line and polyline shapes.
See :doc:`/sculpt_paint/sculpting/introduction/gesture_tools` for more information.

:doc:`/sculpt_paint/sculpting/tools/mask_tools`
   Create a mask via a gesture.

:doc:`/sculpt_paint/sculpting/tools/hide_tools`
   Hides/Shows geometry via a gesture.

:doc:`/sculpt_paint/sculpting/tools/face_set_tools`
   Create a face set via a gesture.

:doc:`/sculpt_paint/sculpting/tools/trim_tools`
   Perform a Boolean operation via a gesture.

:doc:`/sculpt_paint/sculpting/tools/line_project`
   Flatten the geometry towards a drawn line.


Filter Tools
============

.. figure:: /images/sculpt-paint_sculpting_toolbar_filters.png
   :align: right

Tools for applying effects on the entire unmasked and visible mesh.

:doc:`/sculpt_paint/sculpting/tools/mesh_filter`
   Apply a deformation to all unmasked vertices.

:doc:`/sculpt_paint/sculpting/tools/cloth_filter`
   Applies a cloth simulation to all unmasked vertices.

:doc:`/sculpt_paint/sculpting/tools/color_filter`
   Changes the active color attribute on all unmasked vertices.


Single Click Tools
==================

.. figure:: /images/sculpt-paint_sculpting_toolbar_singleclick.png
   :align: right

Simpler tools that apply an operation on surfaces that are clicked on.

:doc:`/sculpt_paint/sculpting/tools/edit_face_set`
   Modifies the face set under the cursor.

:doc:`/sculpt_paint/sculpting/tools/mask_by_color`
   Create a mask from any color from the color attribute by clicking on it.


General Tools
=============

.. figure:: /images/sculpt-paint_sculpting_toolbar_general.png
   :align: right

General transform and annotate tools like in other modes.

:doc:`Move </sculpt_paint/sculpting/tools/transforms>`
   Translation tool.

:doc:`Rotate </sculpt_paint/sculpting/tools/transforms>`
   Rotation tool.

:doc:`Scale </sculpt_paint/sculpting/tools/transforms>`
   Scale tool.

:doc:`Transform </sculpt_paint/sculpting/tools/transforms>`
   Adjust the objects translation, rotations and scale.

:ref:`Annotate <tool-annotate-freehand>`
   Draw free-hand annotation.

   :ref:`Annotate Line <tool-annotate-line>`
      Draw straight line annotation.
   :ref:`Annotate Polygon <tool-annotate-polygon>`
      Draw a polygon annotation.
   :ref:`Annotate Eraser <tool-annotate-eraser>`
      Erase previous drawn annotations.
